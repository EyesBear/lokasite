jQuery(document).ready(function ($) {
  
	var slideCount = $('#slides ul li').length;
	var slideWidth = $('#slides ul li').width();
	var slideHeight = $('#slides ul li').height();
	var sliderUlWidth = slideCount * slideWidth;
	
	$('#slides').css({ width: slideWidth, height: slideHeight });
	
	$('#slides ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
	
    $('#slides ul li:last-child').prependTo('#slides ul');

    function moveLeft() {
        $('#slides ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('#slides ul li:last-child').prependTo('#slides ul');
            $('#slides ul').css('left', '');
        });
    };

    function moveRight() {
        $('#slides ul').animate({
            left: - slideWidth
        }, 200, function () {
            $('#slides ul li:first-child').appendTo('#slides ul');
            $('#slides ul').css('left', '');
        });
    };

    $('a.control_prev').click(function () {
        moveLeft();
    });

    $('a.control_next').click(function () {
        moveRight();
    });

});    

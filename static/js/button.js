
/* THIS CODE NEEDS TO BE PLACED BELOW THE MENU... 
Mr. SPOCK this is LOGICAL...  Yes !!!
You need the HTML ID structure before you can talk to it with JS ... Ah YES!!! CAPTAIN KIRK */

/* display:none
This hides the "burger-menu" ID, and it will not take up any space. 
The element will be hidden, and the page will be displayed as if the element is not there.
*/
document.getElementById("sub-menu").style.display="none";

function ReverseHamburgerMenuDisplay(){
   var id = document.getElementById("sub-menu");
   if(id.style.display == "none") { 
	   id.style.display = "block"; // (un-hides the element) displays as a block element // 
	}else{
	   id.style.display = "none"; 
	}
}

//  MENU ITEMS onmouseover or onmouseout events -- CHANGES BACKGROUND COLOURS //
function HamburgerMenuItemOver(id) { 
id.style.backgroundColor = "#efefef"; 
}

function HamburgerMenuItemOut(id) { 
id.style.backgroundColor = "#fff"; 
}
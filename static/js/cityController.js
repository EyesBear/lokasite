/**
 * Getting Started With AngularJS
 * http://dosoma.com/series/getting-started-with-angularjs
 */

/**
 * Modules are simple containers for various pieces of your application.
 *
 * angular.module('ExampleApp', ['angular-loading-bar'])
 * <body ng-app='ExampleApp'> 
 */
angular.module('CityApp', [])


/**
 * Controller that connects business logic to the view.
 */
.controller('CityController', ['$scope', function($scope) {
	$scope.lokafyers = [{name: 'dave'}, {name: 'carol'}, {name: 'bob'}, {name: 'alice'}, {name: 'elaine'}, {name: 'fred'}];
}]);
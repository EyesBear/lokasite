# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Communication',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('recipient', models.EmailField(default=b'', max_length=75, null=True)),
                ('subject', models.CharField(default=b'', max_length=256, null=True)),
                ('date', models.DateField(null=True)),
                ('content', models.TextField(default=b'', null=True)),
                ('success', models.BooleanField(default=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]

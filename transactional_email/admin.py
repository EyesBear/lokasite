from django.contrib import admin
from .models import Communication

class CommAdmin(admin.ModelAdmin):
	list_display = ('recipient', 'subject')

admin.site.register(Communication, CommAdmin)

from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^contact', views.contact),
    url(r'^city/(?P<city>\w+)/', views.city_info),
    url(r'^lokafyer/(?P<id>\d+)/', views.lokafyer_info),
    url(r'^booking/request', views.booking_request),
    url(r'^booking/payment', views.booking_payment)
]

from django.contrib import admin
from .models import City
from .models import Area
from .models import Interest
from .models import Language
from .models import Lokafyer
from .models import LokafyerArea
from .models import LokafyerLanguage
from .models import LokafyerInterest
from .models import BookingRequest
from .models import BookingRequestLokafyer
from .models import Tour2
from .models import TourLokafyer
from image_cropping import ImageCroppingMixin

class CityAdmin(admin.ModelAdmin):
	list_display = ('city', 'province', 'country')

class AreaAdmin(admin.ModelAdmin):
	search_fields = ['name']
	list_display = ('name', 'city', 'description')

class InterestAdmin(admin.ModelAdmin):
	list_display = ('category',)

class LanguageAdmin(admin.ModelAdmin):
	list_display = ('name', 'level')

class LanguageInline(admin.TabularInline):
	model = LokafyerLanguage

class InterestInline(admin.TabularInline):
	model = LokafyerInterest

class AreaInline(admin.TabularInline):
	model = LokafyerArea

class LokafyerAdmin(ImageCroppingMixin,admin.ModelAdmin):
	list_display = ('first_name', 'last_name', 'status')
	inlines = [LanguageInline, InterestInline, AreaInline]
	fields = [
		'city',
		'first_name', 'last_name', 'date_of_birth', 'email', 'phone', 'age_range', 'photo', 'thumbnail2', 'phrase',
		'languages', 'areas', 'enjoy_day', 'enjoy_night', 'transportation', 'availability', 'about',
		 'alt_phone', 'gender', 'status', 
	]

class LokafyerAreaAdmin(admin.ModelAdmin):
	list_display = ('lokafyer', 'area')

class LokafyerLanguageAdmin(admin.ModelAdmin):
	list_display = ('lokafyer', 'language')

class LokafyerInterestAdmin(admin.ModelAdmin):
	list_display = ('lokafyer', 'interest')

class lokafyerInline(admin.TabularInline):
	model = BookingRequestLokafyer

class BookingRequestAdmin(admin.ModelAdmin):
	list_display = ('first_name', 'last_name', 'destination', 'date_submitted','paid')
	inlines = [lokafyerInline]

class BookingRequestLokafyerAdmin(admin.ModelAdmin):
	list_display = ('bookingrequest', 'lokafyer')

class tourinline(admin.TabularInline):
	model = TourLokafyer

class TourLokafyerAdmin(admin.ModelAdmin):
	list_display = ('city', 'lokafyer')

class Tour2Admin(admin.ModelAdmin):
	list_display = ('request', 'date_submitted')
	inlines = [tourinline]

admin.site.register(City, CityAdmin)
admin.site.register(Area, AreaAdmin)
admin.site.register(Interest, InterestAdmin)
admin.site.register(Language, LanguageAdmin)
admin.site.register(Lokafyer, LokafyerAdmin)
admin.site.register(LokafyerArea, LokafyerAreaAdmin)
admin.site.register(LokafyerLanguage, LokafyerLanguageAdmin)
admin.site.register(LokafyerInterest, LokafyerInterestAdmin)
admin.site.register(BookingRequest, BookingRequestAdmin)
admin.site.register(BookingRequestLokafyer, BookingRequestLokafyerAdmin)
admin.site.register(Tour2, Tour2Admin)
admin.site.register(TourLokafyer, TourLokafyerAdmin)


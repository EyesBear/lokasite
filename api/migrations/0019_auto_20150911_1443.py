# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import smart_selects.db_fields


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0018_auto_20150820_0113'),
    ]

    operations = [
        migrations.CreateModel(
            name='TourLokafyer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('city', models.ForeignKey(to='api.City', null=True)),
                ('lokafyer', smart_selects.db_fields.ChainedForeignKey(chained_model_field=b'city', chained_field=b'city', auto_choose=True, to='api.Lokafyer')),
                ('tour', models.ForeignKey(to='api.Tour', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='tour',
            name='comment',
            field=models.TextField(default=b'', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='tour',
            name='date',
            field=models.DateField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='tour',
            name='date_submitted',
            field=models.DateTimeField(auto_now_add=True, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='tour',
            name='durations',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='tour',
            name='price',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='tour',
            name='time_of_day',
            field=models.CharField(default=b'3', max_length=25, null=True, choices=[(b'morning', b'Morning'), (b'afternoon', b'Afternoon'), (b'evening', b'Evening'), (b'other', b'Other')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tour',
            name='lokafyer',
            field=models.ManyToManyField(to='api.Lokafyer', through='api.TourLokafyer', blank=True),
            preserve_default=True,
        ),
    ]

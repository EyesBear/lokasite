# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0011_auto_20150727_0031'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookingrequest',
            name='date',
            field=models.DateField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookingrequest',
            name='group_type',
            field=models.CharField(default=b'0', max_length=25, null=True, choices=[(b'solo', b'Solo'), (b'family', b'Family'), (b'friends', b'Friends'), (b'collegues', b'Collegues'), (b'lovers', b'Lovers')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookingrequest',
            name='time_of_day',
            field=models.CharField(default=b'3', max_length=25, null=True, choices=[(b'morning', b'Morning'), (b'afternoon', b'Afternoon'), (b'evening', b'Evening'), (b'other', b'Other')]),
            preserve_default=True,
        ),
    ]

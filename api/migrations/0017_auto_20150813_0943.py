# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import image_cropping.fields
import api.models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0016_auto_20150813_0444'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lokafyer',
            name='photo_path',
        ),
        migrations.AddField(
            model_name='lokafyer',
            name=b'thumbnail2',
            field=image_cropping.fields.ImageRatioField(b'photo', '150x150', hide_image_field=False, size_warning=False, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=None, verbose_name='thumbnail2'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='lokafyer',
            name='photo',
            field=image_cropping.fields.ImageCropField(null=True, upload_to=api.models.content_file_name),
            preserve_default=True,
        ),
    ]

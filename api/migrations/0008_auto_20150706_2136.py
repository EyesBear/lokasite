# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0007_auto_20150612_1952'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lokafyer',
            name='phone',
            field=models.CharField(blank=True, help_text=b'Please enter your phone number starting with + and the country code and area code, ex: +14161234567', max_length=15, validators=[django.core.validators.RegexValidator(regex=b'^\\+?1?\\d{9,15}$', message=b"Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")]),
            preserve_default=True,
        )
    ]

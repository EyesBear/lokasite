# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_auto_20150612_1925'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lokafyer',
            name='date_of_birth',
            field=models.DateTimeField(help_text=b'example: 1987-06-29'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='lokafyer',
            name='last_logon',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
    ]

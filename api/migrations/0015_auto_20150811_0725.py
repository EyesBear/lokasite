# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import api.models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0014_auto_20150803_0439'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lokafyer',
            name='address',
        ),
        migrations.RemoveField(
            model_name='lokafyer',
            name='last_logon',
        ),
        migrations.AlterField(
            model_name='lokafyer',
            name='last_name',
            field=models.CharField(default=b'', max_length=64, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='lokafyer',
            name='photo',
            field=models.ImageField(null=True, upload_to=api.models.content_file_name),
            preserve_default=True,
        ),
    ]

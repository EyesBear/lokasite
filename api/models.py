import os
import urllib
from django.db import models
from django.core.validators import RegexValidator
from image_cropping import ImageRatioField, ImageCropField
from easy_thumbnails.files import get_thumbnailer
from django.core.files import File
from smart_selects.db_fields import ChainedForeignKey 

def generate_cache_filename(instance, path, specname, extension):
    return '../test_photo/%s_%s%s' % (instance.pk, specname, extension)

def content_file_name(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (instance.id, ext)
    return os.path.join('original_images', filename)

class City(models.Model):
	'''Represents a city'''
	city = models.CharField(default='', max_length=256, null=True)
	province = models.CharField(default='', max_length=256, null=True)
	country = models.CharField(default='', max_length=256, null=True)
	description = models.TextField(default='', null=True)
        code = models.CharField(max_length=50)

	def __unicode__( self ):
		return "{0}, {1}, {2}".format( self.city, self.province, self.country)

	def to_json (self):
		lokafyers = self.lokafyer_set.all()
		return {'city': self.city,
				'province': self.province,
				'country': self.country,
				'description': self.description,
				'lokafyers': [lokafyer.to_json() for lokafyer in lokafyers]
		}

class Area(models.Model):
	'''Represents an area within a City'''
	name = models.CharField(default='', max_length=256, null=True)
	description = models.TextField(default='', null=True)
	city = models.ForeignKey(City, related_name="areas")
        code = models.CharField(max_length=50)
        geometry = models.CharField(max_length=500)

	def __unicode__( self ):
		return "{0} - {1}".format(self.name, self.city.city)

class Interest(models.Model):
	'''Represents an interest category'''
	category = models.CharField(default='', max_length=256, null=True)

	def __unicode__( self ):
		return "{0}".format(self.category)

class Language(models.Model):
	'''Represents a language and skill level'''

	LEVEL_CHOICES = (('B', 'Beginner'), ('I', 'Intermediate'), ('F', 'Fluent'))

	level = models.CharField(default='B', max_length=1, choices=LEVEL_CHOICES)
	name = models.CharField(default = '', max_length=256, null=True)

	def __unicode__( self ):
		return "{0} ({1})".format(self.name, self.level)


class Lokafyer(models.Model):
	'''Represents a Lokafyer'''

	# Regexs and choices
	phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
	GENDER_CHOICES = (('M', 'Male'), ('F', 'Female'), ('N', 'Not Specified'))
	AGE_RANGE_CHOICES = (('U', 'Under 25'), ('2', '25 - 35'), ('3', '35-45'), ('4', '45-60'), ('O', 'Over 60'), ('5', 'Not specified'))
	
	# fields
	about = models.TextField(default='', null=True, verbose_name="About me", help_text="This would appear on the profile's detailed view. Please include everything you would like travellers to know about you. The more detailed, the better (ex: what you study, what you do for a living, what you enjoy doing, etc.)")
	#address = models.CharField(default='', max_length=256, blank=True)
	age_range = models.CharField(default='5', max_length=1, choices=AGE_RANGE_CHOICES)
	alt_phone = models.CharField(max_length=15, validators=[phone_regex], blank=True)
	areas = models.TextField(default='', null=True, verbose_name='Often seen in:', help_text='Describe the areas of the city you are most familiar with')
	availability = models.TextField(default='', null=True)
	city = models.ForeignKey(City) 
	created_at = models.DateTimeField(auto_now=False, auto_now_add=True)
	date_of_birth = models.DateTimeField(auto_now=False, auto_now_add=False, help_text="example: 1987-06-29")
	email = models.EmailField(blank=True)
	enjoy_day = models.TextField(default='', null=True, verbose_name='On a sunny afternoon, I like to:')
	enjoy_night = models.TextField(default='', null=True, verbose_name='A perfect night out is:')
	first_name = models.CharField(max_length=64, default='', null=True)
	gender = models.CharField(default='N', max_length=1, choices=GENDER_CHOICES)
	languages = models.CharField(max_length=1255, default='', null=True, verbose_name='Languages I Speak', help_text="Please describe level of proficiency: fluent, conversational or beginner")
	#last_logon = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True)
	last_name = models.CharField(max_length=64, default='', blank=True)
	phone = models.CharField(max_length=15, validators=[phone_regex], blank=True, help_text="Please enter your phone number starting with + and the country code and area code, ex: +14161234567")
	photo = ImageCropField(upload_to=content_file_name, null=True)
	thumbnail2 = ImageRatioField('photo', '150x150', allow_fullsize=True)
	
	phrase = models.CharField(max_length=256, default='', null=True, verbose_name="How I would describe myself in one sentence")
	status = models.CharField(max_length = 64, null=True, blank=True)
	transportation = models.TextField(default='', null=True, verbose_name="How you get around ")

	# Intermediate models
	areaRelation = models.ManyToManyField(Area, through='LokafyerArea')
	languageRelation = models.ManyToManyField(Language, through='LokafyerLanguage')
	interestRelation = models.ManyToManyField(Interest, through='LokafyerInterest')

	def __unicode__( self ):
		name = u'{0} {1}'.format(self.first_name, self.last_name)
		return unicode(name)

	def cache(self):
		if self.status != 'Deactivated':
			result = urllib.urlretrieve('static/images/lokafyers/'+str(self.id)+'.jpg')
			self.photo.save(
					os.path.basename('static/images/lokafyers/'+str(self.id)+'.jpg'),
					File(open(result[0]))
					)
			self.save()

	def to_json(self):

		# print get_thumbnailer(self.photo).get_thumbnail({
		# 					'size': (150, 150),
		# 					'box': self.thumbnail2,
		# 					'crop': True,
		# 					'detail': True,
		# 					'bw': True,
		# 				}).url
		# os.remove("aa.txt")
		

		return {'first_name': self.first_name,
				'last_name': self.last_name,
				'age_range': self.age_range,
				'age': self.get_age_range_display(),
				'thumbnail_url' : get_thumbnailer(self.photo).get_thumbnail({
							'size': (150, 150),
							'box': self.thumbnail2,
							'crop': False,
							'detail': True,
							'bw': True,
						}).url,
				'gender': self.gender,
				'about': self.about,
				'about_short': self.phrase,
				'areas': list(set([area.code for area in self.areaRelation.all()])),
				'languages': list(set([language.name for language in self.languageRelation.all()])),
				'interests': list(set([interest.category for interest in self.interestRelation.all()])),
				'id': self.id,
				'status': self.status,
				'lightbox_areas': self.areas,
				'lightbox_languages': self.languages,
				'lightbox_sunny': self.enjoy_day,
				'lightbox_night': self.enjoy_night,
				'lightbox_transportation': self.transportation,
		}


class LokafyerArea(models.Model):
	'''Intermediate model for Lokafyers and the areas they are familiar with'''
	lokafyer = models.ForeignKey(Lokafyer)
	area = models.ForeignKey(Area)

	def __unicode__(self):
		return u"{0} - {1}".format(self.lokafyer, self.area)

class LokafyerLanguage(models.Model):
	'''Intermediate model for Lokafyers and their languages'''
	lokafyer = models.ForeignKey(Lokafyer)
	language = models.ForeignKey(Language)

	def __unicode__(self):
		return u"{0} - {1}".format(self.lokafyer, self.language)

class LokafyerInterest(models.Model):
	'''Intermediate model for Lokafyers and their interests'''
	lokafyer = models.ForeignKey(Lokafyer)
	interest = models.ForeignKey(Interest)

	def __unicode__(self):
		return "{0} - {1}".format(self.lokafyer, self.interest)

class BookingRequest(models.Model):
	'''A booking request.'''

	GROUP_TYPE_CHOICES = (('solo', 'Solo'), 
							('family', 'Family'), 
							('friends', 'Friends'),
							('collegues', 'Collegues'),
							('lovers', 'Lovers'))

	TIME_OF_DAY_CHOICES = (('morning', 'Morning'), ('afternoon', 'Afternoon'), ('evening', 'Evening'), ('other', 'Other'))
	PAID_TYPE_CHOICES = (('Yes', 'Yes'), 
							('No', 'No'), 
							('Refunded', 'Refunded'))
	STATUS_TYPE_CHOICES = (('Confirmed', 'Confirmed'), 
							('Canceled', 'Canceled'), 
							('Tentative', 'Tentative'))

	first_name = models.CharField(default='', max_length=256, null=True)
	last_name = models.CharField(default='', max_length=256, null=True)
	email = models.EmailField()

	group_size = models.IntegerField()
	group_type = models.CharField(default='0', max_length=25, choices=GROUP_TYPE_CHOICES, null=True)
	time_of_day = models.CharField(default='3', max_length=25, choices=TIME_OF_DAY_CHOICES, null=True)
	date = models.DateField(auto_now=False, auto_now_add=False, null=True)
	date_submitted = models.DateTimeField(auto_now=False, auto_now_add=True, null=True)
	destination = models.ForeignKey(City)
	durations=models.IntegerField(null=True)
	# first_lokafyer = models.ForeignKey(Lokafyer, related_name='first_requested_lokafyer', null=True)
	# second_lokafyer = models.ForeignKey(Lokafyer, related_name='second_requested_lokafyer', null=True)
	# third_lokafyer = models.ForeignKey(Lokafyer, related_name='third_requested_lokafyer', null=True)
	about = models.TextField(default='', null=True)
	whattoshow = models.TextField(default='', null=True)
	source=models.TextField(default='', null=True)
	referral=models.TextField(default='', null=True)
	comment=models.TextField(default='', null=True)	
	paid=models.CharField(max_length=25, choices=PAID_TYPE_CHOICES, blank=True)
	payment_source=models.CharField(default='', max_length=256, blank=True)

	price=models.IntegerField(null=True)
	status=models.CharField(max_length=25, choices=STATUS_TYPE_CHOICES, blank=True)
	# Intermediate models
	lokafyerRelation = models.ManyToManyField(Lokafyer, through='BookingRequestLokafyer', blank=True)	

	def __unicode__(self):
		return 'a booking request from: {0} - {1}'.format(self.first_name, self.last_name)



class Payment(models.Model):
	''' A stripe confirmation '''

class Tour2(models.Model):
	''' A confirmed tour '''
	TIME_OF_DAY_CHOICES = (('morning', 'Morning'), ('afternoon', 'Afternoon'), ('evening', 'Evening'), ('other', 'Other'))

	request = models.ForeignKey(BookingRequest)



	# models.ForeignKey(Lokafyer,limit_choices_to={'city':city})
	time_of_day = models.CharField(default='3', max_length=25, choices=TIME_OF_DAY_CHOICES, null=True)
	date = models.DateField(auto_now=False, auto_now_add=False, null=True)
	date_submitted = models.DateTimeField(auto_now=False, auto_now_add=True, null=True)
	durations=models.IntegerField(null=True)
	comment=models.TextField(default='', null=True)	
	price=models.IntegerField(null=True)
	lokafyer = models.ManyToManyField(Lokafyer, through='TourLokafyer', blank=True)

	def __unicode__(self):
		return 'a tour for: {0} '.format(self.request)

class TourLokafyer(models.Model):
	'''Intermediate model for BookingRequests and the Lokafyers they requested'''
	tour = models.ForeignKey(Tour2,null=True)
	city = models.ForeignKey(City, null=True) 
	lokafyer = ChainedForeignKey(
        Lokafyer, 
        chained_field="city",
        chained_model_field="city", 
        show_all=False, 
        auto_choose=True
    )

	def __unicode__(self):
		return "{0} - {1}".format(self.tour, self.lokafyer)

class BookingRequestLokafyer(models.Model):
	'''Intermediate model for BookingRequests and the Lokafyers they requested'''
	bookingrequest = models.ForeignKey(BookingRequest)
	lokafyer = models.ForeignKey(Lokafyer)
	def __unicode__(self):
		return "{0} - {1}".format(self.bookingrequest, self.lokafyer)
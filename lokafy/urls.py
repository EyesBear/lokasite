from django.conf.urls import patterns, include, url
from django.contrib import admin
from pages import views

urlpatterns = patterns('',
    url(r'^$', 'pages.views.index', name='index'),
    url(r'^about/', 'pages.views.about', name='about'),
    url(r'^thanks/', 'pages.views.thanks', name='thanks'),
    url(r'^faqs/', 'pages.views.faqs', name='faqs'),
    url(r'^mission/', 'pages.views.mission', name='mission'),
    url(r'^pricing/', 'pages.views.pricing', name='pricing'),
    url(r'^pricing-paris/', 'pages.views.pricing_paris', name='pricing_paris'),
    url(r'^pricing-toronto/', 'pages.views.pricing_toronto', name='pricing_toronto'),
    url(r'^reviews/', 'pages.views.reviews', name='reviews'),
    url(r'^contact/', 'pages.views.contact', name='contact'),
    url(r'^values/', 'pages.views.values', name='values'),
    url(r'^city/(?P<city>\w+)/', 'pages.views.city', name='city'),
    url(r'^api/', include('api.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^payment/pay', 'payment.views.pay', name='pay'),
    url(r'^submit-profile/', 'pages.views.person_create', name='submit-profile'),
    url(r'^booking_form/', 'pages.views.booking_form', name='booking_form'),
)

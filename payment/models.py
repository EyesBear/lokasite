from django.db import models

# Create your models here.
class StripePayment(models.Model):

	amount = models.IntegerField()
	currency = models.CharField(default='', max_length=4, null=True)
	source = models.CharField(default='', max_length=256, null=True)
	description = models.CharField(default='', max_length=256, null=True)

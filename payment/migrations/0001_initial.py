# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='StripePayment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.IntegerField()),
                ('currency', models.CharField(default=b'', max_length=4, null=True)),
                ('source', models.CharField(default=b'', max_length=256, null=True)),
                ('description', models.CharField(default=b'', max_length=256, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
